// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"



// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToChannels(ECR_Overlap);

}

void AFood::SetFoodType_Implementation()
{
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	int i = rand() % 100;
	if (i <= 50)
	{
		BonusType = EBonus::NOBONUS;
	}
	else if (i <= 80)
	{
		BonusType = EBonus::FASTER;
	}
	else
	{
		BonusType = EBonus::SLOWER;
	}
	SetFoodType();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			switch (BonusType)
			{
			case EBonus::FASTER:
				if (Snake->MovementSpeed > 0.07f)
				{
					Snake->MovementSpeed -= 0.03f;
					Snake->SetActorTickInterval(Snake->MovementSpeed);
				}
				break;
			case EBonus::SLOWER:
				if (Snake->MovementSpeed < 0.3f)
				{
					Snake->MovementSpeed += 0.03f;
					Snake->SetActorTickInterval(Snake->MovementSpeed);
				}
				break;
			}
			this->Destroy();
		}
	}
}

